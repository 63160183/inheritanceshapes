/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author ACER
 */
public class Circle extends Shape{
    private double r;
    private double pi = 22.0/7;
    public Circle(double r){
        super(r,r);
        this.r = r;
        
    }
    @Override
    public double calArea(){
        return pi*r*r;
    }
    @Override
    public void Print(){
        System.out.println("Circle: r: "+ r + " area: " + calArea());
    }
}
