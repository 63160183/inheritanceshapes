/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author ACER
 */
public class Shape {
    protected double x;
    protected double y;
    public Shape(double x, double y){
        System.out.println("shape created");
        this.x = x;
        this.y = y;
    }
    public double calArea(){
        return x*y;
    }
    public void Print(){
        System.out.println("shape: x: " + x + " y: " + y +" area: " + calArea());
    }
}
