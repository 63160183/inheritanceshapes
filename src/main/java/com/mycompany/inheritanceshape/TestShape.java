/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author ACER
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape(2,3);
        shape.Print();
        
        Circle circle = new Circle(3);
        circle.Print();
        
        Circle circle1 = new Circle(4);
        circle1.Print();
        
        Triangle triangle = new Triangle(3,4);
        triangle.Print();
        
        Rectangle rectangle = new Rectangle(4,3);
        rectangle.Print();
        
        Square square = new Square(2);
        square.Print();
        System.out.println("---------");
        
        Shape[] shapes = {circle,circle1,triangle,rectangle,square};
        for(int i=0; i<shapes.length; i++){
            shapes[i].Print();
        }
    }
}
